import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsComponent } from './list/students.component';
import { StudentsAddComponent } from './add/students.add.component';
import { FileNotFoundComponent } from '../shared/file-not-found/file-not-found.component';
import { StudentTableComponent } from './student-table/student-table.component';
const StudentRoutes: Routes = [ 
 { path: 'view', component: StudentsViewComponent },
	 { path: 'add', component: StudentsAddComponent },
		 { path: 'list', component: StudentsComponent},
			 { path: 'detail/:id', component: StudentsViewComponent},
			 { path: 'table', component: StudentTableComponent}
		
		  ];
@NgModule({
  imports: [
    RouterModule.forRoot(StudentRoutes)
  ],
  exports: [
	RouterModule
	]
})
export class StudentRoutingModule { }
